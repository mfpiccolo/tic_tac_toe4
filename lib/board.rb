
class Board
  attr_reader :spaces

  def initialize
    @spaces = {'1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9'}
  end

  def display
    puts "| #{@spaces['1']} | #{@spaces['2']}  | #{@spaces['3']}  |"
    puts "-------------"
    puts "| #{@spaces['4']}  | #{@spaces['5']}  | #{@spaces['6']}  |"
    puts "-------------"
    puts "| #{@spaces['7']}  | #{@spaces['8']}  | #{@spaces['9']}  |"
  end

  def change(player_choice, player)
    if player == "Player 1"
      mark = "X"
    else
      mark = "O"
    end
 
      @spaces[player_choice] = mark
  end

  def check?(player_choice)
    @spaces[player_choice] == "X" || @spaces[player_choice] == "O"
  end
end