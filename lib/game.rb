class Game
attr_reader :turns
  
  def initialize
    @turns = 0
  end

  def over?(spaces)
    @turn == 9 || \
    spaces['1'] == spaces['2'] && spaces['1'] == spaces['3'] || \
    spaces['4'] == spaces['5'] && spaces['4'] == spaces['6'] || \
    spaces['7'] == spaces['8'] && spaces['7'] == spaces['9'] || \
    spaces['1'] == spaces['4'] && spaces['1'] == spaces['7'] || \
    spaces['2'] == spaces['5'] && spaces['2'] == spaces['8'] || \
    spaces['3'] == spaces['6'] && spaces['3'] == spaces['9'] || \
    spaces['1'] == spaces['5'] && spaces['1'] == spaces['9'] || \
    spaces['3'] == spaces['5'] && spaces['3'] == spaces['7']
  end

  def turn
    @turns += 1
  end
end