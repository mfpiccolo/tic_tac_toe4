require './lib/player'
require './lib/game'
require './lib/board'

puts "Welcome to Tic Tac Toe."
game = Game.new
board = Board.new
players = [Player.new("Player 1"), Player.new("Player 2")]

 board.display

unless game.over?(board.spaces)== true 
  player_choice = nil
    players.each do |player|
      puts "#{player.name } please pick a square."
      player_choice = gets.chomp
      board.check?(player_choice)
      board.change(player_choice, player.name)
      board.display
      game.turn
   # make it so you can't override a previous mark
  end
end

# if board.check?(player_choice) = false
#   then puts "Hey!  That space is already filled.  Choose another."
# end