require '../lib/game'

spaces = {'1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9'}

game = Game.new

puts "#{game} should be an instance of Game."

p game

puts "#{game.over?(spaces)} should be false"

spaces = {'1'=>'O', '2'=>'O', '3'=>'O', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9'}

puts "#{game.over?(spaces)} should be true"

