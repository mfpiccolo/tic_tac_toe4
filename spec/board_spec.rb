require '../lib/board'

board = Board.new
puts "#{board} should be an instance of board"

puts "#{board.display} should display the board with begining values"

puts "\n\n*********************************************************"
puts "TESTING PLAYER CHOICE"
player_choice = "5"
board.change(player_choice, "Player 1")
puts "#{board.display} should display the board and X in the middle"

player_choice = "5"
board.change(player_choice, "Player 2")
puts "#{board.display} should display the board and O in the middle"


player_choice = "5"

puts "\n\n*********************************************************"
puts "#{board.check?(player_choice)} should be true"

player_choice = "2"
puts "#{board.check?(player_choice)} should be false"